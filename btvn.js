// Bài 1
/*
Đầu vào : số ngày đã làm 

Lương = số ngày * lương 1 ngày

Đầu ra : tổng số tiền lương 
*/

var soNgay = 7;
var soLuong = 100000;
var tienLuong = soNgay * soLuong;
console.log("", tienLuong);

// Bài 2
/*
Đầu vào : 5 số thực

Giá trị trung bình = tổng 5 số thực / 5

Đầu ra : Giá trị trung bình của 5 số
*/

var num1 = 5;
var num2 = 12;
var num3 = 9;
var num4 = 7;
var num5 = 6;
var trungBinh = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("", trungBinh);

// Bài 3
/*
Đầu vào : số tiền USD 

tổng số tiền VND cần quy đổi = số tiền USD * 23.500

Đầu ra : tổng số tiền VND đã quy đổi
*/

var USDEl = document.getElementById("USD");
var VNDEl = document.getElementById("VND");
function quyDoi() {
  console.log((VNDEl.value = USDEl.value * 23500), "VNĐ");
}

// Bài 4
/* 
Đầu vào : chiều dài và chiều rộng 

Diện tích = chiều dài * chiều rộng
Chu vi = ( chiều dài + chiều rộng ) * 2

Đầu ra : Diện tích, chu vi 
*/

var chieuDai = 14;
var chieuRong = 6;
var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;
console.log("Diện tích : ", dienTich, "m2");
console.log("Chu vi : ", chuVi, "m");

// Bài 5
/*
Đầu vào : 1 số có 2 chữ số

Hàng chục = ký số / 10
Hàng đơn vị = ký số % 10 
Tổng kí số có 2 chữ số = hàng chục + hàng đơn vị

Đầu ra : tổng kí số có 2 chữ số
*/

var number = 69;
var donVi = number % 10;
var hangChuc = Math.floor(number / 10);
var result = donVi + hangChuc;
console.log("", result);
